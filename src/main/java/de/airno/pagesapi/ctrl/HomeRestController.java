package de.airno.pagesapi.ctrl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeRestController {

	public HomeRestController() {
	}

	@GetMapping("/")
	public Map<String, String> sayHello() {
		Map<String, String> body = new HashMap<String, String>();
		body.put("message", "Hello from Spring");
		return body;
	}
}
