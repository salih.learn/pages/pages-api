package de.airno.pagesapi.ctrl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.airno.pagesapi.entity.Page;
import de.airno.pagesapi.entity.Resource;
import de.airno.pagesapi.entity.User;
import de.airno.pagesapi.repo.PageRepository;
import de.airno.pagesapi.repo.ResourceRepository;
import de.airno.pagesapi.repo.UserRepository;

@RestController
@RequestMapping("/u/{username}/p")
public class PagesManagerRestController {

	@Autowired
	private PageRepository pageRepo;
	@Autowired
	private ResourceRepository resourceRepo;
	@Autowired
	private UserRepository userRepo;
	
	public PagesManagerRestController() {
	}
	
	@PostMapping("/")
	public String createPage(@PathVariable String username, @RequestBody Map<String, String> body) throws Exception {
		User user = userRepo.findByUsername(username).get();
		if (user == null) {
			throw new Exception("No such user!");
		}
		// Create page
		Page newPage = new Page();
		newPage.setName(body.get("name"));
		newPage = pageRepo.save(newPage);
		
		// Mark owner
		user.addPage(newPage);
		user = userRepo.save(user);
		
		// Create a folder
		Resource rootResource = new Resource();
		rootResource.setPage(newPage);
		rootResource.setMime(Resource.MIME.FOLDER);
		rootResource.setName(newPage.getName());
		rootResource = resourceRepo.save(rootResource);
		
		// Mark the folder as page's root resource
		newPage.setRootResource(rootResource);
		newPage = pageRepo.save(newPage);
		
		return "Page created with id: " + newPage.getId();
	}

}
