package de.airno.pagesapi.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.airno.pagesapi.entity.Page;
import de.airno.pagesapi.repo.PageRepository;

@RestController
@RequestMapping("/p")
public class PagesRestController {

	@Autowired
	private PageRepository pageRepo;
	
	public PagesRestController() {
	}
	
	@GetMapping("/{pageId}")
	public Page getPageDetails(@PathVariable String pageId) {
		Page page = pageRepo.findById(pageId).get();
		
		// eliminate looped reference
		page.getRootResource().setPage(null);
		
		return page;
	}

}
