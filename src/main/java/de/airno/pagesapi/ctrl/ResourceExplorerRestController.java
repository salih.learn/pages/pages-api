package de.airno.pagesapi.ctrl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.airno.pagesapi.entity.Resource;
import de.airno.pagesapi.repo.ResourceRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/r/{resourceId}")
public class ResourceExplorerRestController {

  @Autowired
  private ResourceRepository resourceRepo;

  public ResourceExplorerRestController() {
  }

  @GetMapping("/")
  public Map<String, Object> getResource(@PathVariable String resourceId) {
    Resource resource = resourceRepo.findById(resourceId).get();
    List<Resource> children = resourceRepo.findByParent(resource);
    List<Map<String, Object>> childArray = new ArrayList<Map<String, Object>>();
    for (Iterator<Resource> iterator = children.iterator(); iterator
        .hasNext();) {
      Resource childResource = (Resource) iterator.next();
      Map<String, Object> child = new HashMap<String, Object>();
      child.put("id", childResource.getId());
      child.put("name", childResource.getName());
      child.put("mime", childResource.getMime());
      childArray.add(child);
    }
    Map<String, Object> resBody = new HashMap<String, Object>();
    resBody.put("id", resource.getId());
    resBody.put("name", resource.getName());
    if (resource.getParent() != null) {
      resource.getParent().setPage(null);
      resource.getParent().setParent(null);
    }
    resource.getPage().setRootResource(null);
    resBody.put("parent", resource.getParent());
    resBody.put("mime", resource.getMime());
    resBody.put("matter", resource.getMatter());
    resBody.put("page", resource.getPage());
    resBody.put("children", childArray);
    return resBody;
  }

}
