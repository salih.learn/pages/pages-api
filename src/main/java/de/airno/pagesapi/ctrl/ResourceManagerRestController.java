package de.airno.pagesapi.ctrl;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.airno.pagesapi.entity.Page;
import de.airno.pagesapi.entity.Resource;
import de.airno.pagesapi.repo.PageRepository;
import de.airno.pagesapi.repo.ResourceRepository;


@RestController
@RequestMapping("/u/{username}/p/{pageId}/r")
public class ResourceManagerRestController {
	
	@Autowired
	private ResourceRepository resourceRepo;
	@Autowired
	private PageRepository pageRepo;
	
	public ResourceManagerRestController() {}

	@DeleteMapping("/{resourceId}")
	public void deleteResource() {}
	
	@PostMapping("/{resourceId}")
	public Map<String, Object> addChildResource(
			@PathVariable String pageId, 
			@PathVariable String resourceId, 
			@RequestBody Map<String, Object> body
			) throws Exception {
		
		String mime = (String) body.get("mime");
		String name = (String) body.get("name");
		
		Page page;
		Resource parent;
		try {
			page = pageRepo.findById(pageId).get();
			parent = resourceRepo.findById(resourceId).get();			
		} catch (NoSuchElementException e) {
			throw new Exception("page or folder does not exist!");
		}
		
		if (!parent.getPage().getId().equals(page.getId())) {
			System.out.println(String.format("Page id mismatch: %s %s", parent.getPage().getId(), page.getId()));
			throw new Exception(
					String.format("401: Resource %s does not belong to page %s",  parent.getId(), page.getId())
					);
		}
		
		Map<String, Object> resBody = new HashMap<String, Object>();
		switch (mime) {
		case Resource.MIME.FOLDER:
			Resource childFolder = addChildFolder(parent, name);
			resBody.put("id", childFolder.getId());
			break;
		case Resource.MIME.WEBURL:
			String url = (String) body.get("url");
			Resource childResource = addChildWebResource(parent, name, url);
			resBody.put("id", childResource.getId());
			break;
		default:
			throw new Exception("Invalid resource type!");
		}
		return resBody;
	}
	
	@PostMapping("/")
	public void copyOrMoveResource() {}
	
	private Resource addChildFolder(Resource parent, String newFolderName) throws Exception {
		if (!parent.isFolder()) {
			throw new Exception("Parent resource should be a folder!");
		}
		Resource newFolder = new Resource();
		newFolder.setMime(Resource.MIME.FOLDER);
		newFolder.setName(newFolderName);
		newFolder.setPage(parent.getPage());
		newFolder.setParent(parent);
		return resourceRepo.save(newFolder);
	}

	private Resource addChildWebResource(Resource parent, String name, String url) throws Exception {
		if (!parent.isFolder()) {
			throw new Exception("Parent resource should be a folder!");
		}
		Resource webResource = new Resource();
		webResource.setMime(Resource.MIME.WEBURL);
		webResource.setName(name);
		webResource.setPage(parent.getPage());
		webResource.setParent(parent);
		Map<String, Object> matter = new HashMap<String, Object>();
		matter.put("url", url);
		webResource.setMatter(matter);
		return resourceRepo.save(webResource);
	}
}
