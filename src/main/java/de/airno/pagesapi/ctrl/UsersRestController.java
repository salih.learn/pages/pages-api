package de.airno.pagesapi.ctrl;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.airno.pagesapi.entity.User;
import de.airno.pagesapi.repo.UserRepository;

@RestController
@RequestMapping("/u")
public class UsersRestController {

	@Autowired
	private UserRepository userRepo;
	
	public UsersRestController() {}
	
	@PostMapping("/")
	public String createUser(@RequestBody Map<String, Object> request) throws Exception {
		String username = (String) request.get("username");
		String email = (String) request.get("email");
		String fullname = (String) request.get("fullname");
		if (userRepo.findByUsername((String) request.get("username")) == null) {
			User newUser = new User(username, email, fullname);
			userRepo.save(newUser);
			return "User added";
		} else {
			throw new Exception("Username taken! use a different username.");
		}
	}
	
	@GetMapping("/{username}")
	public User getUser(@PathVariable String username) throws Exception {
		User user = userRepo.findByUsername(username).get();
		
		// Eliminate endless loop
		user.getPages().forEach(page -> {
			page.getRootResource().setPage(null);
		});
		
		return user;
	}
	
}
