package de.airno.pagesapi.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class Page {

	@Id
	private String id;
	private String name;
	
	@DBRef()
	private Resource rootResource;
	
	public Page(String name, Resource rootResource) {
		super();
		this.name = name;
		this.rootResource = rootResource;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Resource getRootResource() {
		return rootResource;
	}

	public void setRootResource(Resource rootResource) {
		this.rootResource = rootResource;
	}

	public Page() {
		// TODO Auto-generated constructor stub
	}

}
