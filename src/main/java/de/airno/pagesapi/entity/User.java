package de.airno.pagesapi.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class User {
	
	@Id
	private String id;
	private String username;
	private String email;
	private String fullName;
	@DBRef()
	private List<Page> pages;
	
	public String getUsername() {
		return username;
	}

	private void ensurePagesNotNull() {
		if (pages == null) {
			pages = new ArrayList<Page>();
		}
	}
	
	public User(String username, String email, String name) {
		this.username = username;
		this.email = email;
		this.fullName = name;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Page> getPages() {
		return pages;
	}

	public void addPage(Page page) {
		ensurePagesNotNull();
		pages.add(page);
	}
	
	public User() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String name) {
		this.fullName = name;
	}

}
