package de.airno.pagesapi.entity;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class Resource {

	public static final class MIME {
		public static final String FOLDER = "resource/directory";
		public static final String WEBURL = "resource/weburl";
	}
	
	@Id
	private String id;
	private String name;
	@DBRef()
	private Page page;
	@DBRef()
	private Resource parent;
	private String mime;
	private Map<String, Object> matter;
	
	public Resource() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Resource getParent() {
		return parent;
	}

	public void setParent(Resource parent) {
		this.parent = parent;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public Map<String, Object> getMatter() {
		return matter;
	}

	public void setMatter(Map<String, Object> matter) {
		this.matter = matter;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isFolder() {
		if (mime.equals(Resource.MIME.FOLDER)) {
			return true;
		}
		return false;
	}

}
