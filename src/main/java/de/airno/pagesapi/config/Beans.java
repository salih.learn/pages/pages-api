package de.airno.pagesapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;

@Configuration
public class Beans {

	@Value("${spring.data.mongodb.uri}") 
	private String mongoConnectionUrl;
	
	public Beans() {
		// TODO Auto-generated constructor stub
	}
	
	@Bean
	MongoTransactionManager transactionManager(MongoDatabaseFactory dbFactory) {
		return new MongoTransactionManager(dbFactory);
	}
	
}
