package de.airno.pagesapi.repo;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.airno.pagesapi.entity.User;

public interface UserRepository extends MongoRepository<User, String>{
	Optional<User> findByUsername(String username);
}
