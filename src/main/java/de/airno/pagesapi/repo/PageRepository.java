package de.airno.pagesapi.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.airno.pagesapi.entity.Page;

public interface PageRepository extends MongoRepository<Page, String> {

}
