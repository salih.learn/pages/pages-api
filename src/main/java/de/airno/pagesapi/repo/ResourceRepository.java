package de.airno.pagesapi.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.airno.pagesapi.entity.Resource;

public interface ResourceRepository extends MongoRepository<Resource, String> {
	List<Resource> findByParent(Resource parent);
}
